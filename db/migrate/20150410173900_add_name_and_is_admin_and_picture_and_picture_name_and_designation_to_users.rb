class AddNameAndIsAdminAndPictureAndPictureNameAndDesignationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_admin, :boolean, null: false, default: false
    add_column :users, :picture, :string
    add_column :users, :picture_name, :string
    add_column :users, :designation, :string
    add_column :users, :name, :string
  end
end
