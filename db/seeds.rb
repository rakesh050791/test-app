# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
	User.create(name: 'Rakesh' ,email: 'rakesh050791@gmail.com',password: '12345678',password_confirmation:'12345678',is_admin: true,:picture=>"https://www.dropbox.com/s/adp1ysotncnej3o/IMG_20150402_103255.png?dl=0",:designation=>"developer",:picture_name=>"rakesh_developer.jpg")
	User.create(name: 'Admin' ,email: 'admin@letsgomo.com',password: '12345678',password_confirmation:'12345678',is_admin: true,:designation=>"admin",:picture_name=>"admin_admin.jpg",:picture=>"https://www.dropbox.com/s/0q7ue0s71dnsxzx/dummy.jpg?dl=0")