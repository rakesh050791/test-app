require 'nokogiri'
require 'open-uri'

class UsersController < ApplicationController
	def index
		if params[:q]
			@users = User.search(params[:q])
		else
			@users = User.check_user
		end
		respond_to do |format|
      		format.html # index.html.erb 
      		format.js   # index.js.erb  
    	end
	end

	def show	
    @user = User.find(params[:id])
	    respond_to do |format|
	      format.html # show.html.erb	      
	    end
  	end

  	def destroy
    @user = User.find(params[:id])
    @user.destroy

	    respond_to do |format|
	      format.html { redirect_to root_path }	      
	    end
	 end

	 def compose_message
	 	
	 end

	 def send_email
	 	@user = User.find_by_email(params[:email])	 	  
		  path_image = "images/#{@user.picture_name}"		  
		  open(@user.picture) do |chart|
		     File.open(path_image, 'wb') {|f| f.write chart.read }
		  end
	 	 UserMailer.send_message(path_image,@user,params[:subject],params[:body]).deliver
	 	 File.delete(path_image)
	 	respond_to do |format|
	      format.html { redirect_to root_path }	      
	    end
	 end

	def import_data
		doc = Nokogiri::HTML(open("http://letsgomo.com/meet-our-team/"))
			doc.css(".one-fourth").each do |team_member|				
				name = team_member.at_css(".name").text							
				designation = team_member.at_css("p").text
				picture = team_member.at_css(".photo")[:src]
				User.create_records(name,designation,picture)
			end
		redirect_to root_path	
	end

	def new
		@user = User.new
	end

	def create
	  @user = User.new(params[:user])
	  pic_name = params[:user][:name]+"_"+params[:user][:designation]+".jpg"
	  password = params[:user][:name]+"_"+params[:user][:designation]
	  @user.password = password.downcase
	  @user.password_confirmation = password.downcase
	  @user.picture_name = pic_name.downcase
		if @user.save
			UserMailer.send_invitation(@user,password.downcase).deliver
			redirect_to root_path, notice: 'User has been successfully created'
		else
			render 'new'
		end	
	end
end
