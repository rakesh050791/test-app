class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,:name,:picture,:is_admin,:picture_name,:designation
  # attr_accessible :title, :body

  scope :check_user,where(:is_admin => false).order('id desc')
  
  acts_as_messageable

  def self.create_records name,designation,picture
  	email = name+"@letsgomo.com"
  	pic_name = name+"_"+designation+".jpg"
  	User.create(:name=>name,:designation=>designation,:picture=>picture,:picture_name=>pic_name.downcase,:email=>email.downcase,:password=>"12345678")
  end

  def self.search params
    User.where("users.name ILIKE :term OR users.designation ILIKE :term", :term => "%#{params}%" ) 
  end

  # You'd, probably, want to have a separate name column instead
  def name
    email
  end

  def mailboxer_email(object)
    email
  end
  
end
