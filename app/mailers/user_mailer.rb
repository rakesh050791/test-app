class UserMailer < ActionMailer::Base
  default from: "from@example.com"
  def send_message(path_image,user,subject,body)
     attachments["#{user.picture_name}"] = File.read(path_image)
     mail(:to => user.email, :subject => subject, :body=>body, :from => "no-reply@letsgomo.com")
  end

  def send_invitation(user,password)
     @user = user 
     @password = password
     mail(:to => @user.email, :subject => "Account activation",:from => "no-reply@letsgomo.com")
  end
end
